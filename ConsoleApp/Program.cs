﻿using Homework11.WebApi.Entity;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Random rand = new Random();
            HttpClient client = new HttpClient();
            User user = new User() {
                Id = rand.Next(1, 999999999),
                Name = "User" + rand.Next(1,10000),
                Age = rand.Next(18,65)

            };

            var userJson = JsonConvert.SerializeObject(user);
            var httpPostConent = new StringContent(userJson, Encoding.UTF8, "application/json");

            Console.WriteLine("List before:");
            HttpResponseMessage response = await client.GetAsync("https://localhost:44325/user");
            string oldContent = await response.Content.ReadAsStringAsync();
            Console.WriteLine(oldContent);

            Console.WriteLine("Stringified new user:" + userJson);
            response = await client.PostAsync("https://localhost:44325/user", httpPostConent);
            Console.WriteLine("Status:" + response.StatusCode);

            Console.WriteLine("List after updating:");
            response = await client.GetAsync("https://localhost:44325/user");
            string newContent = await response.Content.ReadAsStringAsync();
            Console.WriteLine(newContent);

            int id = 0;
            while (true) {
                Console.WriteLine("Write user ID to recieve");
                id = int.Parse(Console.ReadLine());
                Console.WriteLine("Requested user info:");
                response = await client.GetAsync("https://localhost:44325/user/" + id);
                string requestedContent = await response.Content.ReadAsStringAsync();
                Console.WriteLine(requestedContent);
            }
        }
    }
}
