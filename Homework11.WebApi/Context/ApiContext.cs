﻿using Homework11.WebApi.Entity;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Homework11.WebApi.Context
{
    public class ApiContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public ApiContext()
        {
            Database.EnsureCreated();
        }

        public ApiContext(DbContextOptions opt) : base(opt)
        {
            Database.EnsureCreated();
        }
    }
}
