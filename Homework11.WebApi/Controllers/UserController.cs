﻿using Homework11.WebApi.Context;
using Homework11.WebApi.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Homework11.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ApiContext apiContext;
        public UserController(ApiContext apiContext)
        {
            this.apiContext = apiContext;
        }

        [HttpGet]
        public async Task<User[]> GetUsers()
        {
            using var context = apiContext;
            User[] users = await context.Users.Take(20).ToArrayAsync();
            return users;
        }

        [HttpGet("{id}")]
        public async Task<User> GetUser(int id)
        {
            using var context = apiContext;
            User user = await context.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
            {
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound;
                return null;

            }
            return user;
        }

        [HttpPost]
        public async Task AddUser([FromBody] User user)
        {
            using var context = apiContext;
            User foundUser = await context.Users.FirstOrDefaultAsync(x => x.Id == user.Id);
            if (foundUser != null)
            {
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status409Conflict;
            }
            else
            {
                context.Users.Add(user);
                context.SaveChanges();
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status200OK;
            }
        }
    }
}
